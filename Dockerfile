# STEP 1 build executable binary
FROM golang:alpine as builder
COPY . $GOPATH/src/mypackage/myapp/
WORKDIR $GOPATH/src/mypackage/myapp/
#get dependancies
#you can also use dep
RUN go get -d -v
#build the binary
RUN go build -o /go/bin/hello
# STEP 2 build a small image
# start from scratch
#FROM scratch
# Copy our static executable
#COPY --from=builder /go/bin/hello /go/bin/hello
ENTRYPOINT ["/go/bin/hello"]
